### Useful links
https://technologyconversations.com/2017/06/16/automating-jenkins-docker-setup/
http://tdongsi.github.io/blog/2017/12/30/groovy-hook-script-and-jenkins-configuration-as-code/
https://stackoverflow.com/questions/16963309/how-create-and-configure-a-new-jenkins-job-using-groovy
https://github.com/linagora/james-jenkins/blob/master/create-dsl-job.groovy

### Run

```bash
$ make test
```

Default user: jenkins/pass
Application url: http://localhost:8088

### Check logs

```bash
$ make logs
```

### Destroy environment

```bash
$ make clean
```