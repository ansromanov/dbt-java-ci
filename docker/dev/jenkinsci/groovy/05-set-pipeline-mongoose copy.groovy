import jenkins.model.*
import hudson.plugins.git.*;

println("Create new pipeline job")

def scm = new GitSCM("https://ansromanov@bitbucket.org/ansromanov/jenkins-pipelines.git")
scm.branches = [new BranchSpec("*/master")];

def flowDefinition = new org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition(scm, "mongoose/Jenkinsfile")

def parent = Jenkins.instance
def job = new org.jenkinsci.plugins.workflow.job.WorkflowJob(parent, "mongoose")
job.definition = flowDefinition

job.save();
parent.reload()