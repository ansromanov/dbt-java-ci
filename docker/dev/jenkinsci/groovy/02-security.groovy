import jenkins.model.*
import hudson.security.*
import hudson.security.csrf.DefaultCrumbIssuer
import hudson.tasks.Mailer
import jenkins.security.s2m.AdminWhitelistRule
import org.kohsuke.stapler.StaplerProxy
 
def instance = Jenkins.getInstance()
def hudsonRealm = new HudsonPrivateSecurityRealm(false)

def user = System.getenv('JENKINS_USERNAME') ?: 'jenkins'
def pass = System.getenv('JENKINS_PASSWORD') ?: 'pass'

println("--- Create admin account")
hudsonRealm.createAccount(user, pass)
instance.setSecurityRealm(hudsonRealm)

println("--- Configure security for admin account")
def strategy = new hudson.security.GlobalMatrixAuthorizationStrategy()
strategy.add(Jenkins.ADMINISTER, user)
instance.setAuthorizationStrategy(strategy)

instance.save()
 
println("--- Disable CLI over remoting")
instance.getDescriptor("jenkins.CLI").get().setEnabled(false)

println("--- Enable Slave -> Master Access Control")
instance.getExtensionList(StaplerProxy.class)
    .get(AdminWhitelistRule.class)
    .masterKillSwitch = false

println("--- Checking the CSRF protection")
if (instance.crumbIssuer == null) {
    println "CSRF protection is disabled, Enabling the default Crumb Issuer"
    instance.crumbIssuer = new DefaultCrumbIssuer(true)
}

println("--- Disable outdated protocols (us JNLP4 onlyI)")
instance.agentProtocols = new HashSet<String>(["JNLP4-connect"])
