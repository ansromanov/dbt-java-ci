import jenkins.model.*
import hudson.plugins.git.*;

println("Create new pipeline job")

def scm = new GitSCM("https://github.com/ansromanov/simple-java-maven-app.git")
scm.branches = [new BranchSpec("*/master")];

def flowDefinition = new org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition(scm, "Jenkinsfile")

def parent = Jenkins.instance
def job = new org.jenkinsci.plugins.workflow.job.WorkflowJob(parent, "simple-java-maven-app")
job.definition = flowDefinition

job.save();
parent.reload()